package com.nataliiataran.customertimes.domain
import com.google.gson.annotations.SerializedName


data class Query(
    @SerializedName("done")
    var done: Boolean,
    @SerializedName("nextRecordsUrl")
    var nextRecordsUrl: String,
    @SerializedName("records")
    var records: List<Record>,
    @SerializedName("totalSize")
    var totalSize: Int
)

data class Record(
    @SerializedName("AccountId__c")
    var accountIdC: Any,
    @SerializedName("ConnectionReceivedId")
    var connectionReceivedId: Any,
    @SerializedName("ConnectionSentId")
    var connectionSentId: Any,
    @SerializedName("CreatedById")
    var createdById: String,
    @SerializedName("CreatedDate")
    var createdDate: String,
    @SerializedName("CurrencyIsoCode")
    var currencyIsoCode: String,
    @SerializedName("Description__c")
    var descriptionC: Any,
    @SerializedName("EndDate__c")
    var endDateC: Any,
    @SerializedName("Geolocation__c")
    var geolocationC: Any,
    @SerializedName("Id")
    var id: String,
    @SerializedName("IsApproved__c")
    var isApprovedC: Boolean,
    @SerializedName("IsDeleted")
    var isDeleted: Boolean,
    @SerializedName("IsDone__c")
    var isDoneC: Boolean,
    @SerializedName("IsLocked__c")
    var isLockedC: Boolean,
    @SerializedName("LastActivityDate")
    var lastActivityDate: Any,
    @SerializedName("LastModifiedById")
    var lastModifiedById: String,
    @SerializedName("LastModifiedDate")
    var lastModifiedDate: String,
    @SerializedName("LastReferencedDate")
    var lastReferencedDate: Any,
    @SerializedName("LastViewedDate")
    var lastViewedDate: Any,
    @SerializedName("Name")
    var name: String,
    @SerializedName("OwnerId")
    var ownerId: String,
    @SerializedName("RecordTypeId")
    var recordTypeId: String,
    @SerializedName("StartDate__c")
    var startDateC: Any,
    @SerializedName("Status__c")
    var statusC: String,
    @SerializedName("Subject__c")
    var subjectC: Any,
    @SerializedName("SystemModstamp")
    var systemModstamp: String,
    @SerializedName("attributes")
    var attributes: Attributes
)

data class Attributes(
    @SerializedName("type")
    var type: String,
    @SerializedName("url")
    var url: String
)