package com.nataliiataran.customertimes.domain
import com.google.gson.annotations.SerializedName


data class Describe(
    @SerializedName("actionOverrides")
    var actionOverrides: List<ActionOverride?>? = listOf(),
    @SerializedName("activateable")
    var activateable: Boolean? = false,
    @SerializedName("childRelationships")
    var childRelationships: List<ChildRelationship?>? = listOf(),
    @SerializedName("compactLayoutable")
    var compactLayoutable: Boolean? = false,
    @SerializedName("createable")
    var createable: Boolean? = false,
    @SerializedName("custom")
    var custom: Boolean? = false,
    @SerializedName("customSetting")
    var customSetting: Boolean? = false,
    @SerializedName("deletable")
    var deletable: Boolean? = false,
    @SerializedName("deprecatedAndHidden")
    var deprecatedAndHidden: Boolean? = false,
    @SerializedName("feedEnabled")
    var feedEnabled: Boolean? = false,
    @SerializedName("fields")
    var fields: List<Field?>? = listOf(),
    @SerializedName("hasSubtypes")
    var hasSubtypes: Boolean? = false,
    @SerializedName("isSubtype")
    var isSubtype: Boolean? = false,
    @SerializedName("keyPrefix")
    var keyPrefix: String? = "",
    @SerializedName("label")
    var label: String? = "",
    @SerializedName("labelPlural")
    var labelPlural: String? = "",
    @SerializedName("layoutable")
    var layoutable: Boolean? = false,
    @SerializedName("listviewable")
    var listviewable: Any? = Any(),
    @SerializedName("lookupLayoutable")
    var lookupLayoutable: Any? = Any(),
    @SerializedName("mergeable")
    var mergeable: Boolean? = false,
    @SerializedName("mruEnabled")
    var mruEnabled: Boolean? = false,
    @SerializedName("name")
    var name: String? = "",
    @SerializedName("namedLayoutInfos")
    var namedLayoutInfos: List<Any?>? = listOf(),
    @SerializedName("networkScopeFieldName")
    var networkScopeFieldName: Any? = Any(),
    @SerializedName("queryable")
    var queryable: Boolean? = false,
    @SerializedName("recordTypeInfos")
    var recordTypeInfos: List<RecordTypeInfo?>? = listOf(),
    @SerializedName("replicateable")
    var replicateable: Boolean? = false,
    @SerializedName("retrieveable")
    var retrieveable: Boolean? = false,
    @SerializedName("searchLayoutable")
    var searchLayoutable: Boolean? = false,
    @SerializedName("searchable")
    var searchable: Boolean? = false,
    @SerializedName("supportedScopes")
    var supportedScopes: List<SupportedScope?>? = listOf(),
    @SerializedName("triggerable")
    var triggerable: Boolean? = false,
    @SerializedName("undeletable")
    var undeletable: Boolean? = false,
    @SerializedName("updateable")
    var updateable: Boolean? = false,
    @SerializedName("urls")
    var urls: Urls? = Urls()
)

data class SupportedScope(
    @SerializedName("label")
    var label: String? = "",
    @SerializedName("name")
    var name: String? = ""
)

data class Urls(
    @SerializedName("approvalLayouts")
    var approvalLayouts: String? = "",
    @SerializedName("compactLayouts")
    var compactLayouts: String? = "",
    @SerializedName("defaultValues")
    var defaultValues: String? = "",
    @SerializedName("describe")
    var describe: String? = "",
    @SerializedName("layouts")
    var layouts: String? = "",
    @SerializedName("listviews")
    var listviews: String? = "",
    @SerializedName("quickActions")
    var quickActions: String? = "",
    @SerializedName("rowTemplate")
    var rowTemplate: String? = "",
    @SerializedName("sobject")
    var sobject: String? = "",
    @SerializedName("uiDetailTemplate")
    var uiDetailTemplate: String? = "",
    @SerializedName("uiEditTemplate")
    var uiEditTemplate: String? = "",
    @SerializedName("uiNewRecord")
    var uiNewRecord: String? = ""
)

data class ChildRelationship(
    @SerializedName("cascadeDelete")
    var cascadeDelete: Boolean? = false,
    @SerializedName("childSObject")
    var childSObject: String? = "",
    @SerializedName("deprecatedAndHidden")
    var deprecatedAndHidden: Boolean? = false,
    @SerializedName("field")
    var `field`: String? = "",
    @SerializedName("junctionIdListNames")
    var junctionIdListNames: List<Any?>? = listOf(),
    @SerializedName("junctionReferenceTo")
    var junctionReferenceTo: List<Any?>? = listOf(),
    @SerializedName("relationshipName")
    var relationshipName: String? = "",
    @SerializedName("restrictedDelete")
    var restrictedDelete: Boolean? = false
)

data class RecordTypeInfo(
    @SerializedName("active")
    var active: Boolean? = false,
    @SerializedName("available")
    var available: Boolean? = false,
    @SerializedName("defaultRecordTypeMapping")
    var defaultRecordTypeMapping: Boolean? = false,
    @SerializedName("developerName")
    var developerName: String? = "",
    @SerializedName("master")
    var master: Boolean? = false,
    @SerializedName("name")
    var name: String? = "",
    @SerializedName("recordTypeId")
    var recordTypeId: String? = "",
    @SerializedName("urls")
    var urls: UrlsX? = UrlsX()
)

data class UrlsX(
    @SerializedName("layout")
    var layout: String? = ""
)

data class ActionOverride(
    @SerializedName("formFactor")
    var formFactor: String? = "",
    @SerializedName("isAvailableInTouch")
    var isAvailableInTouch: Boolean? = false,
    @SerializedName("name")
    var name: String? = "",
    @SerializedName("pageId")
    var pageId: String? = "",
    @SerializedName("url")
    var url: Any? = Any()
)

data class Field(
    @SerializedName("aggregatable")
    var aggregatable: Boolean? = false,
    @SerializedName("aiPredictionField")
    var aiPredictionField: Boolean? = false,
    @SerializedName("autoNumber")
    var autoNumber: Boolean? = false,
    @SerializedName("byteLength")
    var byteLength: Int? = 0,
    @SerializedName("calculated")
    var calculated: Boolean? = false,
    @SerializedName("calculatedFormula")
    var calculatedFormula: Any? = Any(),
    @SerializedName("cascadeDelete")
    var cascadeDelete: Boolean? = false,
    @SerializedName("caseSensitive")
    var caseSensitive: Boolean? = false,
    @SerializedName("compoundFieldName")
    var compoundFieldName: Any? = Any(),
    @SerializedName("controllerName")
    var controllerName: Any? = Any(),
    @SerializedName("createable")
    var createable: Boolean? = false,
    @SerializedName("custom")
    var custom: Boolean? = false,
    @SerializedName("defaultValue")
    var defaultValue: Any? = Any(),
    @SerializedName("defaultValueFormula")
    var defaultValueFormula: Any? = Any(),
    @SerializedName("defaultedOnCreate")
    var defaultedOnCreate: Boolean? = false,
    @SerializedName("dependentPicklist")
    var dependentPicklist: Boolean? = false,
    @SerializedName("deprecatedAndHidden")
    var deprecatedAndHidden: Boolean? = false,
    @SerializedName("digits")
    var digits: Int? = 0,
    @SerializedName("displayLocationInDecimal")
    var displayLocationInDecimal: Boolean? = false,
    @SerializedName("encrypted")
    var encrypted: Boolean? = false,
    @SerializedName("externalId")
    var externalId: Boolean? = false,
    @SerializedName("extraTypeInfo")
    var extraTypeInfo: Any? = Any(),
    @SerializedName("filterable")
    var filterable: Boolean? = false,
    @SerializedName("filteredLookupInfo")
    var filteredLookupInfo: Any? = Any(),
    @SerializedName("formulaTreatNullNumberAsZero")
    var formulaTreatNullNumberAsZero: Boolean? = false,
    @SerializedName("groupable")
    var groupable: Boolean? = false,
    @SerializedName("highScaleNumber")
    var highScaleNumber: Boolean? = false,
    @SerializedName("htmlFormatted")
    var htmlFormatted: Boolean? = false,
    @SerializedName("idLookup")
    var idLookup: Boolean? = false,
    @SerializedName("inlineHelpText")
    var inlineHelpText: Any? = Any(),
    @SerializedName("label")
    var label: String? = "",
    @SerializedName("length")
    var length: Int? = 0,
    @SerializedName("mask")
    var mask: Any? = Any(),
    @SerializedName("maskType")
    var maskType: Any? = Any(),
    @SerializedName("name")
    var name: String? = "",
    @SerializedName("nameField")
    var nameField: Boolean? = false,
    @SerializedName("namePointing")
    var namePointing: Boolean? = false,
    @SerializedName("nillable")
    var nillable: Boolean? = false,
    @SerializedName("permissionable")
    var permissionable: Boolean? = false,
    @SerializedName("picklistValues")
    var picklistValues: List<Any?>? = listOf(),
    @SerializedName("polymorphicForeignKey")
    var polymorphicForeignKey: Boolean? = false,
    @SerializedName("precision")
    var precision: Int? = 0,
    @SerializedName("queryByDistance")
    var queryByDistance: Boolean? = false,
    @SerializedName("referenceTargetField")
    var referenceTargetField: Any? = Any(),
    @SerializedName("referenceTo")
    var referenceTo: List<Any?>? = listOf(),
    @SerializedName("relationshipName")
    var relationshipName: Any? = Any(),
    @SerializedName("relationshipOrder")
    var relationshipOrder: Any? = Any(),
    @SerializedName("restrictedDelete")
    var restrictedDelete: Boolean? = false,
    @SerializedName("restrictedPicklist")
    var restrictedPicklist: Boolean? = false,
    @SerializedName("scale")
    var scale: Int? = 0,
    @SerializedName("searchPrefilterable")
    var searchPrefilterable: Boolean? = false,
    @SerializedName("soapType")
    var soapType: String? = "",
    @SerializedName("sortable")
    var sortable: Boolean? = false,
    @SerializedName("type")
    var type: String? = "",
    @SerializedName("unique")
    var unique: Boolean? = false,
    @SerializedName("updateable")
    var updateable: Boolean? = false,
    @SerializedName("writeRequiresMasterRead")
    var writeRequiresMasterRead: Boolean? = false
)