package com.nataliiataran.customertimes.data

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DbOpenHelper(context: Context, columns: List<String>) :
    SQLiteOpenHelper(context, DB_NAME, null, DB_VERSIOM) {

    var columnNames: List<String> = columns
    override fun onCreate(db: SQLiteDatabase?) {
        var COLUMNS = "(" + columnNames[0] + " TEXT PRIMARY KEY, "
        for (i in 1..columnNames.size-2) {
            COLUMNS = COLUMNS + columnNames[i] + " TEXT, "
        }
        COLUMNS = COLUMNS + columnNames[columnNames.size-1] + " TEXT)"
        val CREATE_TABLE = "CREATE TABLE $TABLE_NAME $COLUMNS"
        db?.execSQL(CREATE_TABLE)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db!!.execSQL("DROP TABLE IF EXISTS $TABLE_NAME")
        onCreate(db)
    }

    fun addRecord(columnNames: List<String>, record: HashMap<String, String>): Boolean {
        val db = this.writableDatabase
        val values = ContentValues()
        for (j in columnNames.indices) {
            values.put(columnNames[j], record.get(columnNames[j]))
        }
        db.insert(TABLE_NAME, null, values)

        val _success = db.insert(TABLE_NAME, null, values)
        db.close()
        return (Integer.parseInt("$_success") != -1)
    }

    fun getAllRecords(): List<HashMap<String, String>> {
        val maplist = ArrayList<HashMap<String, String>>()
        val query = "select * from $TABLE_NAME"

        val db = this.writableDatabase
        val cursor = db.rawQuery(query, null)

        if (cursor.moveToFirst()) {
            do {
                val map = HashMap<String, String>()
                for (i in 0 until cursor.columnCount) {
                    map[cursor.getColumnName(i)] = cursor.getString(i)
                }
                maplist.add(map)
            } while (cursor.moveToNext())
        }

        db.close()
        return maplist
    }


    companion object {
        private val DB_NAME = "AccountDB"
        private val DB_VERSIOM = 1
        private val TABLE_NAME = "Account"
    }
}