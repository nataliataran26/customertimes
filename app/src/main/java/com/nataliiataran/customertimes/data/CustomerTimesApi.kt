package com.nataliiataran.customertimes.data

import com.nataliiataran.customertimes.domain.Describe
import com.nataliiataran.customertimes.domain.Query
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.GET

interface CustomerTimesApi {
    @GET("/describe")
    fun fetchDescribe(): Observable<Response<Describe>>

    @GET("/queryAll")
    fun fetchQuery(): Observable<Response<Query>>

}