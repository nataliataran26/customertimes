package com.nataliiataran.customertimes.data

class DataManager(private val restApi: CustomerTimesApi) {
    fun fetchDescribe() = restApi.fetchDescribe()
    fun fetchQuery() = restApi.fetchQuery()
}