package com.nataliiataran.customertimes.presentation

import com.nataliiataran.customertimes.domain.Record

interface MainActivityView : BaseMvpView {
    fun showSuccessMessage()
    fun createColumns(columnNames: List<String>)
    fun setRecords(recordsValue: List<Record>)
    fun showProgress()
    fun hideProgress()
}