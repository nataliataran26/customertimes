package com.nataliiataran.customertimes.presentation

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.nataliiataran.customertimes.R
import com.pawegio.kandroid.layoutInflater

class RecordAdapter : BaseAdapter<RecordAdapter.Holder>() {
    private var list: List<HashMap<String, String>>? = null
    private var columns: List<String>? = null
    fun addItems(columnNames: List<String>, recordList: List<HashMap<String, String>>) {
        list = recordList
        columns = columnNames
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): Holder {
        return Holder(
                parent.context.layoutInflater!!.inflate(
                        R.layout.card_item_record, parent,
                        false
                )
        )
    }

    override fun getItemCount(): Int {
        return list!!.size
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        val recordEntity = list!![position]
        var content = ""
        for (i in columns!!.indices) {
            content = content + columns!![i] + " " + recordEntity.get(columns!![i]) + "\n"
        }
        holder.tvContent.text = content
    }

    inner class Holder(view: View) : RecyclerView.ViewHolder(view) {
        var tvContent: TextView = view.findViewById(R.id.tvContent)
    }
}
