package com.nataliiataran.customertimes.presentation.di.components


import com.nataliiataran.customertimes.presentation.BaseActivity
import com.nataliiataran.customertimes.presentation.MainActivityPresenter
import com.nataliiataran.customertimes.presentations.di.module.AppModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [(AppModule::class)])
interface AppComponent {

    fun inject(baseActivity: BaseActivity)

    fun inject(presenter: MainActivityPresenter)
}