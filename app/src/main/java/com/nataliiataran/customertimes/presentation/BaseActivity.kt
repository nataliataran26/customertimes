package com.nataliiataran.customertimes.presentation

import android.os.Bundle
import com.arellomobile.mvp.MvpAppCompatActivity
import com.nataliiataran.customertimes.App

abstract class BaseActivity : MvpAppCompatActivity(){
    abstract val layoutId: Int

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(layoutId)
        App.appComponent.inject(this)
    }

}