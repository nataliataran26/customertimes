package com.nataliiataran.customertimes.presentation

import com.arellomobile.mvp.InjectViewState
import com.nataliiataran.customertimes.App
import com.nataliiataran.customertimes.domain.Describe
import com.nataliiataran.customertimes.domain.Field
import com.nataliiataran.customertimes.domain.Query
import com.nataliiataran.customertimes.domain.Record
import com.nataliiataran.customertimes.presentation.utils.RxThreadScheduler
import retrofit2.Response

@InjectViewState
class MainActivityPresenter : BasePresenter<MainActivityView>() {

    override fun inject() = App.appComponent.inject(this)

    fun createTable() {
        fetchDescribe()
    }

    private fun fetchDescribe() {
        viewState.showProgress()
        addToComposite(
            dataManager.fetchDescribe()
                .compose(RxThreadScheduler.applySchedules())
                .subscribe({
                    onSuccessDescribe(it)
                }, ::onError)
        )
    }

    private fun onSuccessDescribe(response: Response<Describe>) {
        when (response.code()) {
            200 -> {
                viewState.hideProgress()
                viewState.showSuccessMessage()
                setColumn(response.body()!!.fields!!)
            }
        }
    }

    private fun setColumn(fields: List<Field?>) {
        var columnNames = mutableListOf<String>()
        for (i in fields.indices) {
            columnNames.add(i, fields[i]!!.name!!)
        }
        viewState.createColumns(columnNames)
    }

    fun insertData() {
        fetchQuery()
    }

    private fun fetchQuery() {
        viewState.showProgress()
        addToComposite(
            dataManager.fetchQuery()
                .compose(RxThreadScheduler.applySchedules())
                .subscribe({
                    onSuccessQuery(it)
                }, ::onError)
        )
    }

    private fun onSuccessQuery(response: Response<Query>) {
        when (response.code()) {
            200 -> {
                viewState.hideProgress()
                setRecord(response.body()!!.records)
            }
        }
    }

    private fun setRecord(records: List<Record>) {
        viewState.setRecords(records)
    }

    private fun onError(throwable: Throwable?) {
        viewState.hideProgress()
    }
}

