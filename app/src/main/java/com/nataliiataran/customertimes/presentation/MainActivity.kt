package com.nataliiataran.customertimes.presentation

import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.arellomobile.mvp.presenter.InjectPresenter
import com.nataliiataran.customertimes.R
import com.nataliiataran.customertimes.data.DbOpenHelper
import com.nataliiataran.customertimes.domain.Record
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity(), MainActivityView {
    override val layoutId = R.layout.activity_main
    @InjectPresenter
    lateinit var presenter: MainActivityPresenter
    private var dbOpenHelper: DbOpenHelper? = null
    private val recordAdapter = RecordAdapter()
    lateinit var columnNames: List<String>
    var queryFieldNames = mutableListOf<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initRecyclerView()
        setListeners()
    }

    private fun setListeners() {
        btnCreateTable.setOnClickListener {
            presenter.createTable()
            btnCreateTable.isClickable = false
        }
        btnInsertData.setOnClickListener {
            presenter.insertData()
            btnCreateTable.isClickable = false
        }
        btnShowData.setOnClickListener {
            var recordList = dbOpenHelper!!.getAllRecords()
            recordAdapter.addItems(columnNames, recordList)
            hideButtons()
            rvRecordList.visibility = View.VISIBLE
        }
    }

    private fun hideButtons() {
        btnCreateTable.visibility = View.GONE
        btnInsertData.visibility = View.GONE
        btnShowData.visibility = View.GONE
    }

    private fun initRecyclerView() {
        rvRecordList.adapter = recordAdapter
    }

    override fun createColumns(columnNames: List<String>) {
        dbOpenHelper = DbOpenHelper(this, columnNames)
        this.columnNames = columnNames
        for (i in columnNames.indices) {
            queryFieldNames.add(i, columnNames[i].substring(0, 1).toLowerCase() + columnNames[i].substring(1))
        }
    }

    override fun setRecords(recordsValue: List<Record>) {
        progressBar.visibility = View.VISIBLE
        for (i in recordsValue.indices) {
            val record = hashMapOf<String, String>()
            for (j in columnNames.indices) {
                var item = recordsValue[i]
                var mapItem = "null"
                try {
                    var itemField = item.javaClass.getDeclaredField(queryFieldNames[j])
                    itemField.isAccessible = true
                    if (itemField.get(item) != null) mapItem = itemField.get(item).toString()
                } catch (e: NoSuchFieldException) {
                }
                record[columnNames[j]] = mapItem
            }
            dbOpenHelper!!.addRecord(columnNames, record)
        }
        progressBar.visibility = View.GONE
        Toast.makeText(this, "Inserted Successfully", Toast.LENGTH_LONG).show()
    }

    override fun showSuccessMessage() {
        Toast.makeText(this, "Created Successfully!", Toast.LENGTH_SHORT).show()
    }

    override fun hideProgress() {
        progressBar.visibility = View.GONE
    }

    override fun showProgress() {
        progressBar.visibility = View.VISIBLE
    }
}
