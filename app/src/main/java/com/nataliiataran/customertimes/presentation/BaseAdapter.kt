package com.nataliiataran.customertimes.presentation

import android.support.v7.widget.RecyclerView

abstract class BaseAdapter <VH : RecyclerView.ViewHolder>() : RecyclerView.Adapter<VH>() {

}