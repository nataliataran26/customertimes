package com.nataliiataran.customertimes.presentation

import android.provider.BaseColumns


class RecordContract : BaseColumns {
    companion object {
        val TABLE_NAME = "Account"
        val ID = "Id"
        val NAME = "Name"
    }
}
