package com.nataliiataran.customertimes.presentation
import android.content.Context
import com.arellomobile.mvp.MvpPresenter
import com.nataliiataran.customertimes.data.DataManager
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import rx.Subscription
import rx.subscriptions.CompositeSubscription
import javax.inject.Inject

abstract class BasePresenter<I : BaseMvpView> : MvpPresenter<I>() {
    init {
        inject()
    }

    @Inject
    lateinit var dataManager: DataManager
    @Inject
    lateinit var baseContext: Context
    private val disposables = CompositeDisposable()
    private val compositeSubscription = CompositeSubscription()

    abstract fun inject()

    override fun destroyView(view: I) {
        super.destroyView(view)
        disposables.clear()
        compositeSubscription.clear()
    }

    fun addToComposite(subscription: Subscription) = compositeSubscription.add(subscription)

    fun addToComposite(disposable: Disposable) = disposables.add(disposable)
}