package com.nataliiataran.customertimes.presentation

import android.app.Activity
import android.support.annotation.StringRes
import android.support.v4.app.Fragment
import android.widget.Toast
import com.arellomobile.mvp.MvpView

interface BaseMvpView : MvpView {

    fun toast(@StringRes id: Int) {
        val activity = when (this) {
            is Activity -> this
            is Fragment -> activity
            else -> null
        }
        activity ?: return
        Toast.makeText(activity, id, Toast.LENGTH_SHORT).show()
    }

}
