package com.nataliiataran.customertimes.presentations.di.module


import android.content.Context
import com.nataliiataran.customertimes.data.CustomerTimesApi
import com.nataliiataran.customertimes.data.DataManager
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

@Module(includes = [(RetrofitModule::class)])
class DataModule {

    @Provides
    @Singleton
    fun provideDataManager(restApi: CustomerTimesApi): DataManager {
        return DataManager(restApi)
    }

    @Provides
    @Singleton
    fun provideAuctionApi(retrofit: Retrofit): CustomerTimesApi = retrofit.create(CustomerTimesApi::class.java)

}