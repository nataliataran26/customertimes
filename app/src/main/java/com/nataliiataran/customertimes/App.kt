package com.nataliiataran.customertimes

import android.app.Application
import com.nataliiataran.customertimes.presentation.di.components.AppComponent
import com.nataliiataran.customertimes.presentation.di.components.DaggerAppComponent
import com.nataliiataran.customertimes.presentations.di.module.AppModule


class App : Application() {
    override fun onCreate() {
        super.onCreate()
        //Инициализация дагера------------------------------------------------------------------------------------------
        appComponent = DaggerAppComponent.builder().appModule(
            AppModule(this)
        ).build()
    }


    companion object {
        lateinit var appComponent: AppComponent
    }

}